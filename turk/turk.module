<?php // $Id$

/**
 * @file
 * Mechanical Turk: Allows you to configure a node that will be submitted
 * to mechanical turk for transcription.
 */

$turk_contenttype             = variable_get("turk_contenttype", NULL);
$turk_priceperhit             = variable_get("turk_priceperhit", NULL);
$turk_accesskey               = variable_get("turk_accesskey", NULL);
$turk_accesspass              = variable_get("turk_accesspass", NULL);
$turk_hit_assignment_duration = variable_get("turk_hit_assignment_duration", NULL);
$turk_hit_title               = variable_get("turk_hit_title", NULL);
$turk_hit_desc                = variable_get("turk_hit_desc", NULL);
$turk_hit_lifetime            = variable_get("turk_hit_lifetime", NULL);
$turk_hit_keywords            = variable_get("turk_hit_keywords", NULL);
$turk_URL                     = variable_get("turk_URL", NULL);
$turk_auto_approve            = variable_get("turk_auto_approve", NULL);

if ($turk_contenttype == NULL || $turk_priceperhit == NULL || $turk_accesskey == NULL || $turk_accesspass == NULL || $turk_hit_assignment_duration == NULL || $turk_hit_title == NULL || $turk_hit_desc == NULL || $turk_hit_lifetime == NULL || $turk_hit_keywords == NULL || $turk_URL == NULL || $turk_auto_approve == NULL) {
  drupal_set_message("You must configure " . l('Turk Module', 'admin/settings/turk'), $type = 'warning', $repeat = FALSE);
  variable_set("turk_Config", FALSE);
}
else {
  variable_set("turk_Config", TRUE);
}

require_once("turk_admin.inc");
require_once("turk_xml.inc");

/**
 * Implementation of hook_perm().
 */
function turk_perm() {
  return array('admin turk transcription', 'request turk transcription');
}

/**
 * Implementation of hook_cron().
 *
 * Here we check all of the nodes of the type set in the settings page
 * and take appropriate actions based on the status field.
 */
function turk_cron() {
  if (variable_get("turk_Config", FALSE) == TRUE) {

    print "<pre>\n";

    $node_type = variable_get("turk_contenttype", NULL);
    if (!array_key_exists($node_type, node_get_types())) {
      // node type is not set, or is invalid
      print "Turk node type is not set.\n";
      return;
    }

    $sql = "SELECT  node.title, node.type, node.nid, content_type_" . $node_type .".field_hit_status_value FROM {node}, content_type_" . $node_type ." WHERE node.type = '$node_type' AND node.nid = content_type_" . $node_type .".nid AND node.vid = content_type_" . $node_type .".vid";
    $_checktrans == FALSE;
    $result = db_query($sql);

    while ($anode = db_fetch_object($result)) {
      switch ($anode->field_hit_status_value) {
          case "To Be Transcribed":
              _turk_send_transcription_hit($anode->nid);
              break;
          case "Waiting for Transcription":
              $_checktrans = TRUE;
              break;
      }
    }

    if ($_checktrans == TRUE) {
      _turk_check_for_transcriptions();
    }

    print "</pre>\n";
  }
}

/**
 * Implementation of hook_nodeapi().
 *
 * We hook into the nodeapi to insert the approve / deny buttons into
 * the appropriate node pages
 */
function turk_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if (user_access('admin turk transcription')) {
    $turk_contenttype = variable_get("turk_contenttype", NULL);
    switch (($node->field_hit_status["0"]["value"] == "Transcribed") && ($node->type == $turk_contenttype)) {
      case 'view':
        if ($a4 == 1) {
          if ($x = 1) {  // TODO: What is going on here?
            $approvelink = base_path() .'turk/approve_deny?task=approve&hit='. $node->field_hit_id["0"]["value"] .'&nid='. $node->nid .'&pagecallback='. drupal_get_path_alias(request_uri());
            $denylink    = base_path() .'turk/approve_deny?task=deny&hit='. $node->field_hit_id["0"]["value"] .'&nid='. $node->nid .'&pagecallback='. drupal_get_path_alias(request_uri());
            $message = "You can <a href='$approvelink'>Approve</a> or <a href='$denylink'>Deny</a> this hit.";
            drupal_set_message($message, $type = 'status', $repeat = FALSE);
          }
        }
      break;
    }
  }

/*
  // TODO: Make this configurable. (Do nodes become job on cron, or immediately?)
  if ($op == 'presave') {
    if (($node->field_hit_status["0"]["value"] == "To Be Transcribed") && ($node->type == $turk_contenttype)) {
      // Node was set to be a turk job, make it so.
      _turk_send_transcription_hit($node->id);
    }
  }
*/

}

/**
 * Implementation of hook_menu().
 */
function turk_menu() {

  $items = array();

  $items['admin/settings/turk'] = array(
    'title' => t('Mechanical Turk'),
    'description' => t('Use mechanical turk to perform tasks.'),
    'page callback' => 'turk_admin_page',
    //'page arguments' => array('turk_admin_page'),
    'access arguments' => array('admin turk transcription'),
    'type' => MENU_NORMAL_ITEM,
   );

  //this is added for this current tutorial.
  $items['turk/approve_deny'] = array(
    'title' => t('Mechanical Turk Operation'),
    'page callback' => '_turk_approve_devy',
    'access arguments' => array('request turk transcription'),
    'type' => MENU_CALLBACK
  );

  $items['node/%/set_turk_job'] = array(
    'title' => t('Set node transcription'),
    'page callback' => '_turk_set_job',
    'access arguments' => array('request turk transcription'),
    'page arguments' => array(1),
    'type' => MENU_CALLBACK
  );

  $items['turk/get_hit'] = array(
    'title' => t('Get HIT info'),
    'page callback' => '_turk_get_hit',
    'access arguments' => array('request turk transcription'),
    'type' => MENU_CALLBACK
  );
  $items['turk/dispose_hit'] = array(
    'title' => t('Dispose of HIT'),
    'page callback' => '_turk_dispose_hit',
    'access arguments' => array('request turk transcription'),
    'type' => MENU_CALLBACK
  );

  return $items;

}

/**
 * Get information about a HIT from mturk.com
 */
function _turk_get_hit() {
  if ($_GET['nid']) {
    $nid = $_GET['nid'];
    $node = node_load($nid);
    if ($node->type != variable_get("turk_contenttype", NULL)) {
      drupal_set_message('This node is not enabled for Mechanical Turk jobs. The correct type is ' . variable_get("turk_contenttype", NULL), 'error');
      return '';
    }
    $hit_id = $node->field_hit_id["0"]["value"];
  }
  elseif ($_GET['hit_id']) {
    $hit_id = $_GET['hit_id'];
  }

  $operation        = "GetHIT";
  $additional_params = "&HITId=" . $hit_id;
  $xml              = _turk_get_xml($operation, $additional_params);

  $out = "";
  $out .= "<h2>HIT ID: " . $hit_id . "</h2>";
  $out .= "<pre>";
  $out .= print_r($xml, TRUE);
  $out .= "</pre>";
  $out .= "<a href='turk/dispose_hit?hit_id=" . $hitArray->HITId . "'>Dispose Hit</a>";

  return $out;
}

/**
 * Set a node to be transcribed
 */
function _turk_set_job($nid) {

  $node = node_load($nid);
  if ($node->type != variable_get("turk_contenttype", NULL)) {
    drupal_set_message('This node is not enabled for Mechanical Turk jobs. The correct type is ' . variable_get("turk_contenttype", NULL), 'error');
    return '';
  }

  $node->field_hit_status["0"]["value"] = "To Be Transcribed";
  $node->field_hit_id["0"]["value"] = "";

  node_save($node);

  drupal_set_message('This ' . $node->type . ' will be transcribed by human workers, usually within 24 hours.');
  drupal_goto('node/' . $nid);
  return;
}

/**
 * Get the reviewable hit list and update nodes as nessesary.
 */
function _turk_check_for_transcriptions() {

  //get all reviewable hits
  $operation = "GetReviewableHITs";
  $additional_params = "&PageSize=100";
  $xml = _turk_get_xml($operation, $additional_params);

  //if there's errors we should set a message
  if ($xml->OperationRequest->Errors) {
    drupal_set_message("Mechanical Turk Error:". _turk_print_errors($xml->OperationRequest->Errors->Error), $type = 'error', $repeat = FALSE);
    print "Mechanical Turk Error:". _turk_print_errors($xml->OperationRequest->Errors->Error) . "\n";
  }
  elseif ($xml->GetReviewableHITsResult->NumResults > 0) {

    //if there are results
    print "Turk Reviewable HITs\n";

    $node_type = variable_get("turk_contenttype", NULL);

    //loop through all reviewable hits
    foreach ($xml->GetReviewableHITsResult->HIT as $hitArray) {

      $hit_id = $hitArray->HITId;
      //check the HIT
      $operation = "GetHIT";
      $additional_params = "&HITId=" . $hit_id;
      $xml = _turk_get_xml($operation, $additional_params);

      print "Hit " . $hitArray->HITId . "\n\n";
      print_r($xml);
      print "<a href='turk/dispose_hit?hit_id=" . $hitArray->HITId . "'>Dispose Hit</a>";
      print "\n\n=========\n\n";

      //load the assignment
      $operation = "GetAssignmentsForHIT";
      $additional_params = "&HITId=" . $hit_id;
      $xml = _turk_get_xml($operation, $additional_params);

      //if we suspect the hit has been auto approved
      if ($xml->GetAssignmentsForHITResult->Assignment->AssignmentStatus == "Approved") {

        $answer_xml = simplexml_load_string($xml->GetAssignmentsForHITResult->Assignment->Answer);

        //load the node
        $sql = "SELECT  node.title, node.type, node.nid, content_type_". $node_type .".field_hit_status_value FROM {node}, content_type_". $node_type ." WHERE node.type = '$node_type' AND node.nid = content_type_". $node_type .".nid AND node.vid = content_type_". $node_type .".vid AND content_type_". $node_type .".field_hit_id_value = '$hit_id'";
        $result = db_fetch_object(db_query($sql));

        //update the node
        $node_to_approve = node_load($result->nid);
        $node_to_approve->field_hit_status["0"]["value"] = "Finished";
        $node_to_approve->field_page_number["0"]["value"] = $answer_xml->Answer[1]->FreeText;
        $node_to_approve->field_comments["0"]["value"] = $answer_xml->Answer[2]->FreeText;
        $node_to_approve->body = $answer_xml->Answer[0]->FreeText;
        $node_to_approve->revision = 1;
        node_save($node_to_approve);

        //dispose the hit
        $operation        = "DisposeHIT";
        $additional_params = "&HITId=" . $hit_id;
        $xml              = _turk_get_xml($operation, $additional_params);

        if ($xml->OperationRequest->Errors) {
          drupal_set_message("Mechanical Turk Error:". _turk_print_errors($xml->OperationRequest->Errors->Error), $type = 'warning', $repeat = FALSE);
        }

      }
      else {

        //else update the content of the node
        $answer_xml = simplexml_load_string($xml->GetAssignmentsForHITResult->Assignment->Answer);

        //load the node
        $sql = "SELECT  node.title, node.type, node.nid, content_type_". $node_type .".field_hit_status_value FROM {node}, content_type_". $node_type ." WHERE node.type = '$node_type' AND node.nid = content_type_". $node_type .".nid AND node.vid = content_type_". $node_type .".vid AND content_type_". $node_type .".field_hit_id_value = '$hit_id'";
        $result = db_fetch_object(db_query($sql));

        //update the node
        $node_to_approve = node_load($result->nid);


        $node_to_approve->field_hit_status["0"]["value"] = "Transcribed";
        $node_to_approve->field_page_number["0"]["value"] = $answer_xml->Answer[1]->FreeText;
        $node_to_approve->field_comments["0"]["value"] = $answer_xml->Answer[2]->FreeText;
        $node_to_approve->body = $answer_xml->Answer[0]->FreeText;
        $node_to_approve->revision = 1;
        node_save($node_to_approve);

        // temp code: email us to let us know to do a review
        /*
        global $base_url;
        $email_content  = "Turk job competed and ready for review on site at ". $base_url ."\n\n";
        $email_content .= url('node/' . $node_to_approve->nid, array('absolute'=>TRUE)) . "\n";
        $email_subject = 'Photato Turk Review on' . $base_url;
        mail ('stan@wanderingstan.com', $email_subject, $email_content );
        mail ('taro@petsicon.de',       $email_subject, $email_content );
*/
      }
    }
  }
}

/**
 * Send a node to amazon as a mechanical turk hit
 *
 * @param $nid the node that needs to be converted into the HIT
 */
function _turk_send_transcription_hit($nid) {

  // load our node
  $node = node_load($nid);

  global $base_url;

  //set all the transcription variables
  $operation                    = "CreateHIT";
  $titlenonesc                  = variable_get("turk_hit_title", NULL);
  $title                        = rawurlencode($titlenonesc);
  $description                  = rawurlencode(variable_get("turk_hit_desc", NULL));
  $rewardAmount                 = variable_get("turk_priceperhit", NULL);
  $rewardCurrencyCode           = "USD";
  $assignmentDurationInSeconds  = variable_get("turk_hit_assignment_duration", NULL);
  $lifetimeInSeconds            = variable_get("turk_hit_lifetime", NULL);
  $keywords                     = rawurlencode(variable_get("turk_hit_keywords", NULL));

  $question = variable_get("turk_question", drupal_get_path('module', 'turk') ."/turk_question.xml");

  // if the "question" is a filepath, load it.
  if (file_exists($question)) {
    $question = file_get_contents($question);
  }
  // eval any php in the question
  ob_start();
  eval('?>' . $question);
  $question = ob_get_contents();
  ob_end_clean();


  $turk_qual_value              = variable_get("turk_qual_value", NULL);
  $turk_qual_id                 = variable_get("turk_qual_id", NULL);
  $turk_qual_operator           = variable_get("turk_qual_operator", NULL);
  $turk_auto_approve            = variable_get("turk_auto_approve", NULL);
  //set up qualification
  if (!empty($turk_qual_id)) {
    $qualifications = "&QualificationRequirement.1.QualificationTypeId=". $turk_qual_id
      ."&QualificationRequirement.1.Comparator=". $turk_qual_operator
      ."&QualificationRequirement.1.IntegerValue=". $turk_qual_value;
  }

  //setup the parameters
  $additional_params = "&Title=" . $title
    . "&Description=" . $description
    . "&Reward.1.Amount=" . $rewardAmount
    . "&Reward.1.CurrencyCode=" . $rewardCurrencyCode
    . "&Question=" . urlencode($question)
    . $qualifications
    . "&AssignmentDurationInSeconds=" . $assignmentDurationInSeconds
    . "&AutoApprovalDelayInSeconds=" . $turk_auto_approve
    . "&LifetimeInSeconds=" . $lifetimeInSeconds
    . "&Keywords=" . $keywords;

  //do the operation
  $xml = _turk_get_xml($operation, $additional_params);

  //check for errors
  if ($xml->OperationRequest->Errors) {
    drupal_set_message("Mechanical Turk Error:". _turk_print_errors($xml->OperationRequest->Errors->Error), $type = 'warning', $repeat = FALSE);
  }
  else if ($xml->HIT->HITId) {
    $node->field_hit_id["0"]["value"] = $xml->HIT->HITId;
    $node->field_hit_status["0"]["value"] = "Waiting for Transcription";
    $node->field_page_number["0"]["value"] = "";
    $node->field_comments["0"]["value"] = "";
    $node->body = "";
    $node->revision = 1;
    node_save($node);
    //print "Set up node for transcription. Node ID:" . $node->nid . " HIT ID: " . $xml->HIT->HITId . "\n<br>";
    //print_r($xml);
  }
  else {
    drupal_set_message(t("Mechanical Turk: there was an unspecified error.  Possibly mhash, curl, or libxml2 related."), $type = 'error', $repeat = FALSE);
    if (user_access('admin turk transcription')) {
      drupal_set_message('Mechanical Turk Response<br><pre>' . print_r($xml, TRUE) . '</pre>', $type = 'error');
    }

  }
}

/**
 * Approve an assignment in amazone mechanical turk
 *
 * @param $assignment_id the mechanical turk assignment id $hit_id the mechanical turk HIT id
 * @return TRUE for sucess FALSE for failure
 */
function _turk_send_transcription_approval($assignment_id, $hit_id) {

  //approve the assignment
  $operation        = "ApproveAssignment";
  $additional_params = "&assignment_id=" . $assignment_id;
  $xml              = _turk_get_xml($operation, $additional_params);
  if ($xml->OperationRequest->Errors) {
    return FALSE;
  }

  //dispose of the hit
  $operation        = "DisposeHIT";
  $additional_params = "&HITId=" . $hit_id;
  $xml              = _turk_get_xml($operation, $additional_params);
  if ($xml->OperationRequest->Errors) {
    return FALSE;
  }
  else{
    return TRUE;
  }
}

/**
 * reject an assignment in amazone mechanical turk
 *
 * @param $assignment_id the mechanical turk assignment id $hit_id the mechanical turk HIT id
 * @return TRUE for sucess FALSE for failure
 */
function _turk_send_transcription_denial($assignment_id, $hit_id, $nid) {

  //dispose the hit
  $operation        = "DisposeHIT";
  $additional_params = "&HITId=" . $hit_id;
  $xml              = _turk_get_xml($operation, $additional_params);
  if ($xml->OperationRequest->Errors) {
    return FALSE;
  }

  //create the new hit and update the node body and fields
  _turk_send_transcription_hit($nid);
  return TRUE;
}

/**
 * dispose an assignment in amazone mechanical turk
 *
 * @param $assignment_id the mechanical turk assignment id  $hit_id the mechanical turk HIT id
 * @return TRUE for sucess FALSE for failure
 */
function _turk_dispose_HIT($hit_id = '') {

  if ($_GET['hit_id']) {
    $hit_id = $_GET['hit_id'];
  }

  //dispose the hit
  $operation        = "DisposeHIT";
  $additional_params = "&HITId=" . $hit_id;
  $xml              = _turk_get_xml($operation, $additional_params);

  return krumo_ob($xml) . "<hr>" . print_r($xml, TRUE);
  if ($xml->OperationRequest->Errors) {
    return FALSE;
  }
  else{
    return TRUE;
  }
}

/**
 * A page callback for the page that approves or denys mechanical turk assignments
 */
function _turk_approve_devy() {

  //only procees if we have all the params
  if (!empty($_REQUEST["hit"]) && !empty($_REQUEST["nid"]) && !empty($_REQUEST["pagecallback"]) && !empty($_REQUEST["task"])) {

    $hit_id            = $_REQUEST["hit"];
    $nid              = $_REQUEST["nid"];
    $pagecallback     = $_REQUEST["pagecallback"];
    $operation        = "GetAssignmentsForHIT";
    $additional_params = "&HITId=". $hit_id;

    //get the assignment id
    $xml = _turk_get_xml($operation, $additional_params);
    $assignment_id = $xml->GetAssignmentsForHITResult->Assignment->assignment_id;

    //call the approve or reject actions
    if ($_REQUEST["task"] == "approve") {
      if (_turk_send_transcription_approval($assignment_id, $hit_id)) {
        $transcription_node = node_load($nid);
        $transcription_node->field_hit_status["0"]["value"] = "Finished";
        node_save($transcription_node);
      }
      else {
        drupal_set_message(t("There was an error approving your assignment."), $type = 'warning', $repeat = FALSE);
      }
    }
    else if ($_REQUEST["task"] == "deny") {
      if (!_turk_send_transcription_denial($assignment_id, $hit_id, $nid)) {
        drupal_set_message(t("There was an error denying your assignment."), $type = 'warning', $repeat = FALSE);
      }
    }

    //save the node and redirect to the callback
    $redirURL = "http://".preg_replace("/^(\.)?([^.]*\..*)$/", "$2", $_SERVER['HTTP_HOST']).$_REQUEST["pagecallback"];
    //$redirURL = "http://192.168.0.5/". $_REQUEST["pagecallback"];
    header("Location: $redirURL");
  }
  else {
    echo t("No parameters passed.");
  }
}
