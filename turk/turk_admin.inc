<?php // $Id

/**
 * @file
 * File contains all of the administration related hooks for the module
 */

/**
 * Implementation of hook_help().
 */
function turk_help($path, $arg) {
  $output = '';
  switch ($path) {
    case "admin/help#turk":
      $output = '<p>'.  t("Provides functionality to a content type to perform mechanical turk operations on nodes.") .'</p>';
      break;
  }
  return $output;
}

/**
 * Implementation of hook_admin_page().
 */
function turk_admin_page() {

  $output = '<p>'.t('To configure this mechanical Turk module you must select a content type below that has the following fields set up:<br><br>

    field_hit_status(TEXT:Checkboxed/RadioButtons, 1 Value, Allowed Values: Do Not Transcribe, To Be Transcribed, Waiting for Transcription, Transcribed, Finished, Error)<br>
    field_image(Image, jpg jpeg png gif) <em>(Not required if using image module instead of imagefield module)</em><br>
    field_hit_id(TEXT, 50)<br>
    field_page_number(TEXT, 10)<br>
    field_comments(TEXT, 200)<br><br>').'</p>';

  //get the account balance
  $operation = "GetAccountBalance";
  $xml = _turk_get_xml($operation);
  if($xml->GetAccountBalanceResult->Request && $xml->GetAccountBalanceResult->Request->Errors) {
    $output .= "Error Retrieving Balance:". _turk_print_errors($xml->GetAccountBalanceResult->Request->Errors->Error)."</p>";
  }

  //display the balance
  $balance = $xml->GetAccountBalanceResult->AvailableBalance->FormattedPrice;
  if($balance) {
    $output .= "<p>". t("Your Account balance: ") . $balance ."</p>";
  }

  $output .= drupal_get_form('turk_admin');

  return $output;

}

/**
 * Implementation of hook_admin().
 */
function turk_admin() {

  $turk_contenttype             = variable_get("turk_contenttype", NULL);
  $turk_priceperhit             = variable_get("turk_priceperhit", '0.27');
  $turk_accesskey               = variable_get("turk_accesskey", NULL);
  $turk_accesspass              = variable_get("turk_accesspass", NULL);
  $turk_hit_assignment_duration = variable_get("turk_hit_assignment_duration", '7200');
  $turk_hit_title               = variable_get("turk_hit_title", 'Transcribe text from handwritten pages');
  $turk_hit_desc                = variable_get("turk_hit_desc", 'Given a scanned image of a page type in the text');
  $turk_hit_lifetime            = variable_get("turk_hit_lifetime", '604800');
  $turk_hit_keywords            = variable_get("turk_hit_keywords", 'image, letter, diary, transcribe, transcription');
  $turk_URL                     = variable_get("turk_URL", 'http://mechanicalturk.sandbox.amazonaws.com/onca/xml');
  $turk_qual_id                 = variable_get("turk_qual_id", '000000000000000000L0');
  $turk_qual_operator           = variable_get("turk_qual_operator", 'GreaterThan');
  $turk_qual_value              = variable_get("turk_qual_value", '90');
  $turk_auto_approve            = variable_get("turk_auto_approve", '2592000');
  $turk_question = variable_get("turk_question", drupal_get_path('module', 'turk') ."/turk_question.xml");

  $form['#post'];

  $form['turk'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mechanical Turk Settings'),
    '#weight' => 1,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['turk']['turk_URL'] = array(
  '#type' => 'textfield',
  '#title' => t('REST Interface URL'),
  '#size' => 100,
  '#maxlength' => 100,
  '#default_value' => $turk_URL,
  '#description' => t("Allows you to switch from sandbox to production.<br/>sandbox: http://mechanicalturk.sandbox.amazonaws.com/onca/xml<br/>live site: http://mechanicalturk.amazonaws.com/onca/xml. "),
  '#required' => TRUE,
  );
  $form['turk']['turk_contenttype'] = array(
  '#type' => 'select',
  '#title' => t('Select the content type'),
  '#options' => node_get_types('names'),
  '#default_value' => $turk_contenttype,
  '#required' => TRUE,
  );

  $form['turk']['turk_accesskey'] = array(
  '#type' => 'textfield',
  '#title' => t('Access Key'),
  '#size' => 50,
  '#default_value' => $turk_accesskey,
  '#maxlength' => 50,
  '#description' => t("Your mechanical turk access key. <a target='mturk' href='http://aws-portal.amazon.com/gp/aws/developer/account/index.html?action=access-key'>Get your key</a>"),
  '#required' => TRUE,
  );
  $form['turk']['turk_accesspass'] = array(
  '#type' => 'textfield',
  '#title' => t('Access Password'),
  '#default_value' => $turk_accesspass,
  '#size' => 50,
  '#maxlength' => 50,
  '#description' => t("Your mechanical turk access secret"),
  '#required' => TRUE,
  );
  $form['hit'] = array(
    '#type' => 'fieldset',
    '#title' => t('HIT Settings'),
    '#weight' => 2,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['hit']['turk_hit_title'] = array(
  '#type' => 'textfield',
  '#title' => t('HIT Title'),
  '#default_value' => $turk_hit_title,
  '#size' => 50,
  '#maxlength' => 50,
  '#description' => t("The title you would like to use for the HIT <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/CreateHITOperation.html'>API</a>"),
  '#required' => TRUE,
  );

  $form['hit']['turk_priceperhit'] = array(
  '#type' => 'textfield',
  '#title' => t('Price per HIT'),
  '#size' => 5,
  '#maxlength' => 5,
  '#default_value' => $turk_priceperhit,
  '#description' => t("The price you will pay per HIT <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/CreateHITOperation.html'>API</a>"),
  '#required' => TRUE,
  );
  $form['hit']['turk_hit_desc'] = array(
  '#type' => 'textfield',
  '#title' => t('HIT Description'),
  '#default_value' => $turk_hit_desc,
  '#size' => 100,
  '#maxlength' => 100,
  '#description' => t("The description you would like to use for the HIT <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/CreateHITOperation.html'>API</a>"),
  '#required' => TRUE,
  );
  $form['hit']['turk_hit_keywords'] = array(
  '#type' => 'textfield',
  '#title' => t('HIT Keywords'),
  '#default_value' => $turk_hit_keywords,
  '#size' => 100,
  '#maxlength' => 100,
  '#description' => t("The keywords you would like to use for the HIT <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/CreateHITOperation.html'>API</a>"),
  '#required' => TRUE,
  );
  $form['hit']['turk_hit_lifetime'] = array(
  '#type' => 'textfield',
  '#title' => t('HIT Lifetime'),
  '#default_value' => $turk_hit_lifetime,
  '#size' => 10,
  '#maxlength' => 10,
  '#description' => t("The life time in seconds you would like to use for the HIT <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/CreateHITOperation.html'>API</a>"),
  '#required' => TRUE,
  );
  $form['hit']['turk_hit_assignment_duration'] = array(
  '#type' => 'textfield',
  '#title' => t('HIT Assignment Duration'),
  '#default_value' => $turk_hit_assignment_duration,
  '#size' => 10,
  '#maxlength' => 10,
  '#description' => t("The duration in seconds you would like to use for the Assignment <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/CreateHITOperation.html'>API</a>"),
  '#required' => TRUE,
  );
  $form['hit']['turk_auto_approve'] = array(
  '#type' => 'textfield',
  '#title' => t('HIT Auto Approve'),
  '#default_value' => $turk_auto_approve,
  '#size' => 10,
  '#maxlength' => 10,
  '#description' => t("The duration in seconds you would like to use for the Auto Approval of the Assignment (System Default: 2592000) <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/CreateHITOperation.html'>API</a>"),
  '#required' => TRUE,
  );
  $form['hit']['turk_question'] = array(
  '#type' => 'textarea',
  '#title' => t('HIT Question'),
  '#default_value' => $turk_question,
  '#rows' => 5,
  '#description' => t("The question to ask the turk workers, in correct XML format. PHP values may be used, referencing the current \$node. Alternatively, you can specify the path to a file containing the question."),
  '#required' => TRUE,
  );

  $form['qual'] = array(
    '#type' => 'fieldset',
    '#title' => t("Worker Qualifications"),
    '#weight' => 2,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['qual']['turk_qual_id'] = array(
  '#type' => 'textfield',
  '#title' => t('Qualification Type Id '),
  '#default_value' => $turk_qual_id,
  '#size' => 50,
  '#maxlength' => 50,
  '#description' => t("The system specified qualification id <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/QualificationRequirementDataStructureArticle.html'>API</a>"),
  '#required' => TRUE,
  );
  $form['qual']['turk_qual_operator'] = array(
  '#type' => 'textfield',
  '#title' => t('Qualification Operator'),
  '#default_value' => $turk_qual_operator,
  '#size' => 50,
  '#maxlength' => 50,
  '#description' => t("The qualification operator <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/QualificationRequirementDataStructureArticle.html'>API</a>"),
  '#required' => TRUE,
  );
  $form['qual']['turk_qual_value'] = array(
  '#type' => 'textfield',
  '#title' => t('Qualification Value'),
  '#default_value' => $turk_qual_value,
  '#size' => 50,
  '#maxlength' => 50,
  '#description' => t("The qualification value <a href='http://docs.amazonwebservices.com/AWSMechanicalTurkRequester/2005-10-01/ApiReference/QualificationRequirementDataStructureArticle.html'>API</a>"),
  '#required' => TRUE,
  );


  return system_settings_form($form);
}

/**
 * Implementation of hook_admin_validate().
 */
function turk_admin_validate($form, &$form_state) {

  $priceperhit = $form_state['values']['turk_priceperhit'];

  if (!is_numeric($priceperhit)) {
    form_set_error('turk_priceperhit', t('You must select a number for the price per hit.'));
  }
  else if ($priceperhit <= 0) {
    form_set_error('turk_priceperhit', t('Price per hit must be positive.'));
  }
}
