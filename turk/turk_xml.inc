<?php // $Id

/**
 * @file
 * File contains all of the Amazon REST helper functions
 */

/**
 * Generate a amazon formatted timestamp
 *
 * @param $time the curent time
 * @return the amazon formatted timestamp
 */
function _turk_generate_timestamp($time) {
  return gmdate("Y-m-d\TH:i:s\\Z", $time);
}

/**
 * This function replaces mhash
 *
 * @param $key the amazon secret access key $s an amazone operation signature
 * @return a encryped hash of the parameters
 */
function _turk_hmac_sha1($key, $s) {
  return pack("H*", sha1((str_pad($key, 64, chr(0x00)) ^ (str_repeat(chr(0x5c), 64))) .
                         pack("H*", sha1((str_pad($key, 64, chr(0x00)) ^ (str_repeat(chr(0x36), 64))) . $s))));
}

/**
 * Generate a amazon signature
 *
 * @param $service the amazon service name $operation operation to perform 
 * $timestamp a timestamp $secret_access_key the amazon secret access key
 * @return an amazon encrypted signature
 */
function _turk_generate_signature($service, $operation, $timestamp, $secret_access_key) {
  $string_to_encode = $service . $operation . $timestamp;
  $hmac = _turk_hmac_sha1($secret_access_key, $string_to_encode);
  $signature = base64_encode($hmac);
  return $signature;
}

/**
 * Prints out amazon errors from an xml callback
 * @param $error_nodes a portion of the amazon xml callback containing the errors 
 * @return formatted error message
 */
function _turk_print_errors($error_nodes) {
  $out = "There was an error processing your request:<br>\n";
  foreach ($error_nodes as $error) {
    $out .= "  Error code:    " . $error->Code . "\n" . "  Error message: <blockquote>" . $error->Message . "</blockquote>\n";
  }
  return $out;
}

/**
 * Function does the actual REST call to amazon using CURL
 *
 * @param $operation the REST operation to perform $additionalparams addition POST variables
 * formatted like they were GET variables
 * @return xml result or set drupal error messages
 */
function _turk_get_xml($operation, $additionalparams = "") {
  
	$AWS_ACCESS_KEY_ID      = variable_get("turk_accesskey", NULL);
	$AWS_SECRET_ACCESS_KEY  = variable_get("turk_accesspass", NULL);	
	$SERVICE_NAME           = "AWSMechanicalTurkRequester";
	$SERVICE_VERSION        = "2007-03-12";
	$timestamp              = _turk_generate_timestamp(time());
	$signature              = _turk_generate_signature($SERVICE_NAME, $operation, $timestamp, $AWS_SECRET_ACCESS_KEY);
	$url1                   = variable_get("turk_URL", NULL);

	//$url1 = "http://mechanicalturk.sandbox.amazonaws.com/onca/xml";

	$url2 = "Service=" . urlencode($SERVICE_NAME)
	  . "&Operation=" . urlencode($operation)
	  . $additionalparams
	  . "&Version=" . urlencode($SERVICE_VERSION)
	  . "&Timestamp=" . urlencode($timestamp)
	  . "&AWSAccessKeyId=" . urlencode($AWS_ACCESS_KEY_ID)
	  . "&Signature=" . urlencode($signature);

	$session = curl_init($url1);
	curl_setopt($session, CURLOPT_POST, TRUE);
	curl_setopt($session, CURLOPT_POSTFIELDS, $url2);
	curl_setopt($session, CURLOPT_HEADER, FALSE);
	curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
	$response = curl_exec($session);

	curl_close($session); 
	$xml = simplexml_load_string($response);

	if ($xml->OperationRequest->Errors) {
	  drupal_set_message("Mechanical Turk Error:". _turk_print_errors($xml->OperationRequest->Errors->Error), $type = 'warning', $repeat = FALSE);
	}
	
	return $xml;
}